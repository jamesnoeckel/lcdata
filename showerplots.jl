using Gadfly

include("lcioanalyze.jl")
filename = ARGS[1]
plotpath = ARGS[2]

df = makeshowerframe(filename)

# p = plot(df, x="Energy", color="Particle", Geom.density, Scale.x_log10)
# Gadfly.draw(PNG("$(plotpath)energy-by-particle.png", 18cm, 18cm), p)
p = plot(df, y="Energy", x="Angle", color="Particle")
Gadfly.draw(PNG("$(plotpath)energy-vs-angle.png", 18cm, 18cm), p)

# p = plot(df, x="MeshEccentricity", y="VolumeRatio", xgroup="Particle", Geom.subplot_grid(Scale.x_continuous(minvalue=0, maxvalue=1), Scale.y_continuous(minvalue=0, maxvalue=1), Geom.histogram2d(xbincount=30, ybincount=30)))
# Gadfly.draw(PNG("$(plotpath)VolumeRatio_vs_MeshEccentricity.png", 36cm, 9cm), p)

# p = plot(df, x="Particle", Geom.histogram)
# Gadfly.draw(PNG("$(plotpath)Particle_distribution.png", 24cm, 9cm), p)
